import random
import pygame
from pygame.locals import *


def indeks(A, B):
    """Funkcja tworząca z dwóch list jedną listę indeksów"""
    return [a + b for a in A for b in B]

litery = 'ABCDEFGHI'
cyfry = '123456789'

"""Tworzenie komórek, wierszy, kolumn, bloków 3x3"""
komorka = indeks(litery, cyfry)
wiersz = [indeks(lit, cyfry) for lit in litery]
kolumna = [indeks(litery, cyf) for cyf in cyfry]
blok = [indeks(lit3, cyf3) for lit3 in ('ABC', 'DEF', 'GHI') for cyf3 in ('123', '456', '789')]
sudoku = wiersz + kolumna + blok


"""Informacja o tym, gdzie znajduje się dana komórka
 oraz w jakich innych komórkach nie może powtarzać się jej wartość"""
polozenie = dict((i, [pol for pol in sudoku if i in pol]) for i in komorka)
sasiedzi = dict((i, set(sum(polozenie[i],[])) - set([i])) for i in komorka)

"""Przypisywanie komórkom wartości, początkowo ustawiamy zero"""
wartosc = dict((i, 0) for i in komorka)

"""Cechy komorek"""
odpowiedz = dict((i, None) for i in komorka)
czyrozwiazane = dict((i, False) for i in komorka)

def przypisz(kom,war,mozl):
    """Przypisywanie wartości (war) komórce (kom) spośród dostępnych możliwości(mozl)"""
    war[kom] = random.choice(mozl[kom])
    return war[kom]

def redukuj(a, list):
    """Usuwanie liczby z listy możliwości"""
    if a in list:
        list.remove(a)

wolnek = komorka.copy()
mozliwosci = dict((i, [1,2,3,4,5,6,7,8,9]) for i in wolnek)
def generator():
    """Generator sudoku - każdej komórce przypisuje wartość spośród danych możliwości,
    wybierając w pierwszej kolejności komórki z najmniejszą liczbą możliwości"""
    while len(wolnek) != 0:
        liczbamozliwosci = dict((i, len(mozliwosci[i])) for i in wolnek)
        m = min(liczbamozliwosci.values())
        najkrotsze = {}
        for i in wolnek:
            if liczbamozliwosci[i] == m:
                najkrotsze.update({i: liczbamozliwosci[i]})
        y = random.choice(list(najkrotsze))
        wartosc[y] = przypisz(y, wartosc, mozliwosci)
        for i in sasiedzi[y]:
            if wartosc[y] == odpowiedz[i]:
                wartosc[y] = 0
            else:
                odpowiedz[y] = wartosc[y]
                czyrozwiazane[y] = True
                for j in sasiedzi[y]:
                    if czyrozwiazane[j] == False:
                        redukuj(wartosc[y], mozliwosci[i])
        wolnek.remove(y)


wyswietlane = dict((i, 'nie') for i in komorka)
def plansza():
    """Funkcja planszy - wybiera losowo 27 komórek, które będą wyświetlane na starcie"""
    for i in range(0, 27):
        i = random.choice(komorka)
        wyswietlane[i] = 'tak'

"""Właściwości ekranu"""
background_colour = (25, 25, 112)
secondbackground = (255, 255, 255)
(width, height) = (700, 500)
lightblue = (94, 94, 154)
cyfry_gracza = (0, 0, 128)
white =(255, 255, 255)

pygame.font.init()
tytul_czcionka = pygame.font.SysFont('Segoe Script', 36)
menu_czcionka = pygame.font.SysFont('Segoe Script', 26)

screen = pygame.display.set_mode((width, height))

gameIcon = pygame.image.load('asg.png')
pygame.display.set_icon(gameIcon)

def przycisk(napis, x, y, szer, wys, k1, k2, screen, akcja=None):
    """Funkcja przyjmuje tekst, wymiary, kolory i tworzy przycisk wywołujący inną, daną funkcję"""
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    if x+szer > mouse[0] > x and y+wys > mouse[1] > y:
        pygame.draw.rect(screen, k2,(x,y,szer,wys))

        if click[0] == 1 and akcja != None:
            akcja()
    else:
        pygame.draw.rect(screen, k1,(x,y,szer,wys))

    screen.blit(napis, (x+20, y+20))

def wspolrzedne(i):
    """Funkcja podająca współrzędne danej komórki, konkretniej położenie tekstu w komórce"""
    x = 55
    sz = 65
    wy = -50
    listawektorow = []
    for b in range(1, 10):
        for a in range(1, 10):
            listawektorow.append((a * x, b * x))
    punkt = ((listawektorow[i])[0] + sz, (listawektorow[i])[1] + wy)
    return punkt


def wyswietlcyfre(tekst, i, kolor):
    """Funkcja przyjmuje cyfrę w formie string, indeks komórki i kolor.
    Wyświetla daną cyfrę w odpowiedniej komórce tabeli"""
    cyfra_czcionka = pygame.font.SysFont('Segoe Script', 28)
    cyfra = cyfra_czcionka.render(tekst, True, kolor)
    screen.blit(cyfra, wspolrzedne(i))


aktywne = dict((i, False) for i in komorka)
def proba(i):
    """Funkcja, która pobiera informacje o naciśnięciu myszy i wpisaniu cyfry na klawiaturze
    Jeśli wpisana cyfra zgadza się z odpowiedzią w danej komórce, cyfra jest wyświetlana"""
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()
    if wyswietlane[i] == 'nie':
        x = komorka.index(i)
        y = wspolrzedne(x)
        a = (y[0]-11, y[1])
        active = aktywne[i]
        szer = 45
        wys = 45

        if a[0] + szer > mouse[0] > a[0] and a[1] + wys > mouse[1] > a[1]:
            pygame.draw.rect(screen, lightblue, (a[0], a[1], szer, wys))
            if click[0] == 1:
                active = True
        else:
            pygame.draw.rect(screen, white, (a[0], a[1], szer, wys))

        if active == True:
            odpgracza = ""
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.key == K_1 or event.key == K_KP1:
                        odpgracza += "1"
                    elif event.key == K_2 or event.key == K_KP2:
                        odpgracza += "2"
                    elif event.key == K_3 or event.key == K_KP3:
                        odpgracza += "3"
                    elif event.key == K_4 or event.key == K_KP4:
                        odpgracza += "4"
                    elif event.key == K_5 or event.key == K_KP5:
                        odpgracza += "5"
                    elif event.key == K_6 or event.key == K_KP6:
                        odpgracza += "6"
                    elif event.key == K_7 or event.key == K_KP7:
                        odpgracza += "7"
                    elif event.key == K_8 or event.key == K_KP8:
                        odpgracza += "8"
                    elif event.key == K_9 or event.key == K_KP9:
                        odpgracza += "9"
                    elif event.key == K_BACKSPACE:
                        odpgracza = odpgracza[:-1]
            if odpgracza == str(odpowiedz[i]):
                wyswietlane[i] = 'tak'


def hint():
    """Funkcja wybiera losową komórkę i wyświetla jej wartość na ekranie"""
    lista = []
    for i in komorka:
        if wyswietlane[i] == 'nie':
            lista.append(i)
    x = random.choice(lista)
    wyswietlane[x] = 'tak'


def zakonczenie():
    """Funkcja wyświetla napis kończący grę i przycisk umożliwiający rozpoczęcie nowej gry"""
    tekstkoniec = tytul_czcionka.render("Congratulations!", True, white)
    pygame.draw.rect(screen, background_colour, (230, 130, 335, 100))
    screen.blit(tekstkoniec, (240, 140))
    tekstnewgame = tytul_czcionka.render("New game", True, white)
    przycisk(tekstnewgame, 230, 220, 335, 100, lightblue, background_colour, screen, nowa_gra)


def nowa_gra():
    """Funkcja rozpoczynająca nową grę"""
    reset_wartosc()
    reset_odp()
    reset_czyrozw()
    reset_wolnek()
    reset_mozliwosci()
    reset_wyswietlane()
    generator()
    plansza()
    ekrangry()

def reset_wartosc():
    """Funkcja przywracająca każdej komórce wartość 0"""
    wartosc = dict((i, 0) for i in komorka)
    return wartosc

def reset_odp():
    """Funkcja przywracająca każdej komóce odpowiedź = none"""
    odpowiedz = dict((i, None) for i in komorka)
    return odpowiedz

def reset_czyrozw():
    """Funkcja ustawiająca każdą komórkę z powrotem jako nierozwiązaną"""
    czyrozwiazane = dict((i, False) for i in komorka)
    return czyrozwiazane

def reset_wolnek():
    """Funkcja tworząca kopię listy komorka"""
    wolnek = komorka.copy()
    return wolnek

def reset_mozliwosci():
    """Funkcja przywracająca każdej komórce wszystkie możliwości"""
    mozliwosci = dict((i, [1, 2, 3, 4, 5, 6, 7, 8, 9]) for i in wolnek)
    return mozliwosci

def reset_wyswietlane():
    """Funkcja ustawiająca wszystkie komórki jako niewyświetlane"""
    wyswietlane = dict((i, 'nie') for i in komorka)
    return wyswietlane


def oautorze():
    """Funkcja wyświetlająca ekran z informacjami o autorze gry"""
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('A Sudoku Game')
    screen.fill(background_colour)

    text = menu_czcionka.render('Julia Leszczyńska', True, white)
    text2 = menu_czcionka.render('Fizyka Techniczna - Fizyka Stosowana', True, white)
    text3 = menu_czcionka.render('Politechnika Gdańska 2018', True, white)
    text4 = menu_czcionka.render('Back', True, white)
    screen.blit(text, (150, 10))
    screen.blit(text2, (100, 150))
    screen.blit(text3, (150, 350))

    pygame.display.update()
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        przycisk(text4, 150, 400, 100, 90, background_colour, lightblue, screen, ekranstartowy)
        pygame.draw.rect(screen, white, (148, 398, 104, 94), 2)

        pygame.display.update()


def ekrangry():
    """Funkcja wyświetlająca główny ekran gry"""
    print(odpowiedz)
    background_colour = (25, 25, 112)
    secondbackground = white
    cyfry_gracza = (0, 0, 128)
    (width, height) = (700, 500)

    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('A Sudoku Game')
    screen.fill(secondbackground)
    text_hint = menu_czcionka.render('Hint', True, (0, 0, 0))

    """Rysowanie tabeli"""
    pygame.draw.rect(screen, (0, 0, 0), (102, 1, 495, 495), 5)  # główny zarys
    pygame.draw.line(screen, (0, 0, 0), (157, 1), (157, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (212, 1), (212, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (267, 1), (267, 496), 2)
    pygame.draw.line(screen, (0, 0, 0), (322, 1), (322, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (377, 1), (377, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (432, 1), (432, 496), 2)
    pygame.draw.line(screen, (0, 0, 0), (487, 1), (487, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (542, 1), (542, 496), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 56), (597, 56), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 111), (597, 111), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 166), (597, 166), 2)
    pygame.draw.line(screen, (0, 0, 0), (102, 221), (597, 221), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 276), (597, 276), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 331), (597, 331), 2)
    pygame.draw.line(screen, (0, 0, 0), (102, 386), (597, 386), 1)
    pygame.draw.line(screen, (0, 0, 0), (102, 441), (597, 441), 1)

    pygame.display.update()
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        for i in komorka:
            if wyswietlane[i] == 'tak':
                x = komorka.index(i)
                wyswietlcyfre(str(odpowiedz[i]), x, (0,0,0))
            proba(i)
            pygame.display.flip()

        przycisk(text_hint, 5, 60, 92, 80, white, lightblue, screen, hint)

        if ('nie' in wyswietlane.values()) == False:
            zakonczenie()


def ekranstartowy():
    """Funkcja wyświetlająca ekran startowy"""
    LEFT = 1

    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('A Sudoku Game')
    screen.fill(background_colour)

    text_tytul = tytul_czcionka.render('A Sudoku Game', True, white)
    text_start = menu_czcionka.render('New Game', True, white)
    text_author = menu_czcionka.render('Author', True, white)
    text_exit = menu_czcionka.render('Exit', True, white)
    screen.blit(text_tytul,(200,0))
    pygame.draw.rect(screen, white,(228, 128, 182, 102), 2)
    pygame.draw.rect(screen, white, (228, 228, 182, 102), 2)
    pygame.draw.rect(screen, white, (228, 328, 182, 102), 2)

    pygame.display.flip()
    running = True
    while running:
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN and event.button == LEFT:
                print("You pressed the left mouse button at (%d, %d)" % event.pos)
            elif event.type == pygame.MOUSEBUTTONUP and event.button == LEFT:
                print("You released the left mouse button at (%d, %d)" % event.pos)

        przycisk(text_start, 230, 130, 180, 98, background_colour, lightblue, screen, nowa_gra)
        przycisk(text_author, 230, 230, 180, 98, background_colour, lightblue,screen, oautorze)
        przycisk(text_exit, 230, 330, 180, 98, background_colour, lightblue, screen, pygame.QUIT)

        pygame.display.update()

ekranstartowy()